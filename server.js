const { Http2ServerResponse } = require('http2');

var express = require('express')();
var http = require('http').createServer(express);

var io = require('socket.io')(http);


http.listen(8000, () => {
    console.log("Listening on port 8000")
});